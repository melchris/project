<?php

use App\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Eductation',
        ]);
        Category::create([
            'name' => 'Drama',
        ]);
        Category::create([
            'name' => 'Advantures and Fantasy',
        ]);
        Category::create([
            'name' => 'Thriller and Horror',
        ]);
        Category::create([
            'name' => 'Fiction',
        ]);
        Category::create([
            'name' => 'Sci-Fi(Science Fiction)',
        ]);
        Category::create([
            'name' => 'Short Stories',
        ]);
        Category::create([
            'name' => 'Hobbies',
        ]);
    }
}
