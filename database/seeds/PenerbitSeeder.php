<?php

use App\Penerbit;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PenerbitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Penerbit::create([
            'nama' => 'Gramedia Pustaka Utama',
            'alamat' => 'Kompas Gramedia, Jl. Palmerah Barat, RT.1/RW.2, Gelora, Tanah Abang, Kota Jakarta Pusat'
        ]);
        Penerbit::create([
            'nama' => 'Mizan Pustaka',
            'alamat' => 'Jalan Cinambo No. 135, Cisaranten Wetan, Bandung'
        ]);
        Penerbit::create([
            'nama' => 'Bentang Pustaka',
            'alamat' => 'Jl. Palagan Tentara Pelajar, No.101, Jongkang RT 004 RW 035, Sariharjo, Ngaglik, Sleman'
        ]);
        Penerbit::create([
            'nama' => 'Penerbit Erlangga',
            'alamat' => 'Jl. H. Baping No. 100, Ciracas Jakarta Timur'
        ]);
        Penerbit::create([
            'nama' => 'Penerbit Republika',
            'alamat' => 'Jl. Kav. Polri J No.65, RT.6/RW.6, Jagakarsa, Kec. Jagakarsa, Kota Jakarta Selatan'
        ]);

    }
}
