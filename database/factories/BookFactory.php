<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Book;
use App\Category;
use App\Penerbit;
use App\Penulis;
use Faker\Generator as Faker;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'judul' => $faker->text($maxNbChars = 45),
        'tahun_terbit' => $faker->numberBetween($min = 1950, $max = 2000),
        'content' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'penulis_id' => Penulis::all()->random()->id,
        'category_id' => Category::all()->random()->id,
        'penerbit_id' => Penerbit::all()->random()->id,

    ];
});
