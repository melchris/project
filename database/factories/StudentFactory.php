<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Student;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'nama' => $faker->firstName . ' ' . $faker->lastName,
        'alamat' => $faker->address,
        'kelas' => $faker->numberBetween(7, 12),
    ];
});
