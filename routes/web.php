<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/', function() {
//     return view('halaman.index');
// });

Route::middleware('auth')->group(function () {
    Route::resource('/penerbit', PenerbitController::class);
    Route::resource('/penulis', PenulisController::class);
    // Route::resource('/category', 'CategoryController');
    Route::resource('/category', CategoryController::class);
    // Route::resource('/book', 'BookController');
    Route::resource('/book', BookController::class);
    Route::resource('/student', StudentController::class);
    Route::resource('/pinjam', StudentController::class);
    Route::resource('/student', 'StudentController');
    Route::resource('/pinjam', 'PinjamController');
    Route::get('/pinjam/{id}/return', 'PinjamController@kembali');
    Route::post('/pinjam/{id}/return', 'PinjamController@kembalisave');
});

Route::get('/', 'DashboardController@index');

Auth::routes();

// Route::get('/master', function(){
//     return view('layout.master');
// });

// Route::resource('/book', 'BookController');

// Route::resource('/category', 'CategoryController');

// Route::resource('/category', CategoryController::class);
