<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penerbit extends Model
{
    protected $table = 'penerbit';

    protected $guarded = ['id'];

    public function book() //table Kategori memiliki banyak berita
    {
        return $this->hasMany('App\Book');
    }
}
