<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Book;
use App\Category;
use Illuminate\Support\Facades\Storage;

class BookController extends Controller
{
    public function index()
    {
        $book = Book::all();
        return view('book.index', compact('book'));
    }

    public function create()
    {
        $category = DB::table('category')->get();
        $penulis = DB::table('penulis')->get();
        $penerbit = DB::table('penerbit')->get();

        return view('book.create', compact('category', 'penulis', 'penerbit'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'tahun_terbit' => 'required',
            'content' => 'required',
            'penulis_id' => 'required',
            'category_id' => 'required',
            'penerbit_id' => 'required',
            'thumbnail' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $thumbnailName = time().'.'.$request->thumbnail->extension(); //image dibuat unique

        $request->thumbnail->move(public_path('gambar'), $thumbnailName);

        $book = new Book;

        $book->judul = $request->judul;
        $book->tahun_terbit = $request->tahun_terbit;
        $book->content = $request->content;
        $book->penulis_id = $request->penulis_id;
        $book->category_id = $request->category_id;
        $book->penerbit_id = $request->penerbit_id;
        $book->thumbnail = $thumbnailName;

        $book->save();

        return redirect()->route('book.index');
    }

    public function show($id)
    {
        $book = Book::findOrFail($id);

        return view('book.show', compact('book'));
    }

    public function edit($id)
    {
        $category = DB::table('category')->get();
        $penulis = DB::table('penulis')->get();
        $penerbit = DB::table('penerbit')->get();

        $book = Book::findOrFail($id);

        return view('book.edit', compact('book', 'category', 'penulis', 'penerbit'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'tahun_terbit' => 'required',
            'content' => 'required',
            'penulis_id' => 'required',
            'category_id' => 'required',
            'penerbit_id' => 'required',
            'thumbnail' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $book = Book::find($id);

        if($request->has('thumbnail')) {
            $thumbnailName = time().'.'.$request->thumbnail->extension(); //image dibuat unique

            $request->thumbnail->move(public_path('gambar'), $thumbnailName);

            $book->judul = $request->judul;
            $book->tahun_terbit = $request->tahun_terbit;
            $book->content = $request->content;
            $book->penulis_id = $request->penulis_id;
            $book->category_id = $request->category_id;
            $book->penerbit_id = $request->penerbit_id;
            $book->thumbnail = $thumbnailName;
        } else {
            $book->judul = $request->judul;
            $book->content = $request->content;
            $book->category_id = $request->category_id;
        }
        $book->update();

        return redirect('/book/' . $book->id);
    }

    public function destroy($id)
    {
        $book= Book::find($id);


        $path ="gambar/";
        Storage::delete($path . $book->thumbnail);
        // File::delete($path . $book->thumbnail);
        $book->delete();

        return redirect('/book');
    }
}
