<?php

namespace App\Http\Controllers;

use App\Book;
use App\Student;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PinjamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::all();
        $books = Book::all();
        $students = Student::all();
        $user = Auth::user();
        // dd($transactions);
        return view('pinjam.index', compact(['transactions', 'students', 'books', 'user']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'student_id' => 'required',
            'book_id' => 'required',
            'user_id' => 'required',
            'tanggal_pinjam' => 'required|date',
            'durasi' => 'required|integer',
        ]);
        Transaction::create($request->all());
        return redirect()->route('pinjam.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Transaction::findorfail($id);
        // dd($transaction);

        return view('pinjam.show', compact('transaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaction = Transaction::findorfail($id);
        $books = Book::all();
        $students = Student::all();
        $user = Auth::user();
        return view('pinjam.edit', compact(['transaction', 'books', 'students', 'user']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transaction = Transaction::findorfail($id);
        $request->validate([
            'student_id' => 'required',
            'book_id' => 'required',
            'tanggal_pinjam' => 'required|date',
            'durasi' => 'required|integer',
        ]);
        // dd($request, $transaction);
        $transaction->update($request->all());
        return redirect()->route('pinjam.show', $transaction);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction = Transaction::findorfail($id);
        $transaction->delete();
        return redirect()->route('pinjam.index');
    }

    public function kembali($id)
    {
        $transaction = Transaction::findorfail($id);
        $books = Book::all();
        $students = Student::all();
        $user = Auth::user();
        return view('pinjam.kembali', compact(['transaction', 'books', 'students', 'user']));
    }

    public function kembalisave(Request $request, $id)
    {
        $request->validate([
            'tanggal_kembali' => 'required'
        ]);
        $transaction = Transaction::findorfail($id);
        // dd($request, $transaction);
        $transaction->update([
            'tanggal_kembali' => $request->tanggal_kembali,
        ]);
        // dd($transaction, $request);
        return redirect()->route('pinjam.index');
    }
}
