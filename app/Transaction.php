<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $guarded = ['id'];

    public function peminjam() {
        return $this->belongsTo('App\Student', 'student_id');
    }

    public function buku() {
        return $this->belongsTo('App\Book', 'book_id');
    }

    public function petugas() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function status()
    {
        if ($this->tanggal_kembali == NULL ) {
            return "Belum Kembali";
        }
        return "Sudah Kembali";
    }
}
