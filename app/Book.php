<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'book';

    protected $guarded = ['id'];

    public function penerbit() {
        return $this->belongsTo('App\Penerbit');
    }

    public function penulis() {
        return $this->belongsTo('App\Penulis');
    }

    public function category() {
        return $this->belongsTo('App\Category');
    }

    public function pinjam() {
        return $this->belongsTo('App\Pinjam');
    }

    public function user() //table Kategori memiliki banyak berita
    {
        return $this->hasMany('App\User');
    }

    public function student() //table Kategori memiliki banyak berita
    {
        return $this->hasMany('App\Student');
    }

    // Mengembalikan gambar Thumbnail jika ada dan cover default jika kolom Thumbnail kosong
    public function thumb() {
        return $this->thumbnail ?? 'coverdefault.jpg';
    }
}
