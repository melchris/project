<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pinjam extends Model
{
    protected $table = 'pinjam';

    protected $guarded = ['id'];

    public function book() //table Kategori memiliki banyak berita
    {
        return $this->hasMany('App\Book');
    }
}
