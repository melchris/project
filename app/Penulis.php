<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penulis extends Model
{
    protected $table = 'penulis';

    protected $guarded = ['id'];
    
    public function book() //table Kategori memiliki banyak berita
    {
        return $this->hasMany('App\Book');
    }
}
