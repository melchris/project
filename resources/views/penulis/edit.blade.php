@extends('layout.master')

@section('judul')
    Edit Penulis
@endsection

@push('script')
    <script src="{{ asset('admin/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('admin/plugins/select2/css/select2.css') }}"></script>
    <script>
        $(function() {
            $("#select2").select2();
        });
    </script>
@endpush

@section('content')


    <div class="card my-3">
        <div class="card-body">

            <form action="/penulis/{{ $penulis->id }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">Nama penulis</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="colFormLabel" placeholder=" " name="nama"
                            value="{{ $penulis->nama }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">Alamat penulis</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="colFormLabel" placeholder=" " name="alamat"
                            value="{{ $penulis->alamat }}">
                    </div>
                </div>
                <button class="btn btn-success" type="submit">Edit</button>
            </form>


        </div>
    </div>


@endsection
