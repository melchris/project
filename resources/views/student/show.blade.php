@extends('layout.master')

@section('judul')
    Detail Siswa
@endsection

@push('script')
    <script src="{{ asset('admin/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('admin/plugins/select2/css/select2.css') }}"></script>
    <script>
        $(function() {
            $("#select2").select2();
        });
    </script>
@endpush

@section('content')
    {{-- Button menghapus Siswa --}}
    <div class="btn-group">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalHapusSiswa">
            Hapus
        </button>
        <a class="btn btn-secondary" href="/student/{{ $student->id }}/edit">Edit</a>
    </div>

    {{-- Modal untuk menghapus Siswa --}}
    <div class="modal fade" id="modalHapusSiswa" tabindex="-1" aria-labelledby="modalHapusSiswaLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalHapusSiswaLabel">Hapus Siswa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/student/{{ $student->id }}" method="POST">
                    <div class="modal-body">
                        @csrf
                        @method('DELETE')
                        Yakin akan menghapus siswa?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Yakin Hapus</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- End modal untuk menghapus siswa --}}


    <div class="card my-3">
        <div class="card-body">

            <div class="form-group row">
                <label for="colFormLabel" class="col-sm-2 col-form-label">Nama Siswa</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="colFormLabel" placeholder=" "
                        value="{{ $student->nama }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="colFormLabel" class="col-sm-2 col-form-label">Alamat Siswa</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="colFormLabel" placeholder=" "
                        value="{{ $student->alamat }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="colFormLabel" class="col-sm-2 col-form-label">Kelas Siswa</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="colFormLabel" placeholder=" "
                        value="{{ $student->kelas }}" disabled>
                </div>
            </div>


        </div>
    </div>


@endsection
