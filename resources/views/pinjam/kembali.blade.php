@extends('layout.master')

@section('judul')
    Masukkan Pengembalian
@endsection

@push('script')
    <script src="{{ asset('admin/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('admin/plugins/select2/css/select2.css') }}"></script>
    <script>
        $(function() {
            $("#select2").select2();
        });
    </script>
@endpush

@section('content')

    <div class="card my-3">
        <div class="card-body">
            <form action="/pinjam/{{ $transaction->id }}/return" method="POST">
                @csrf
                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">Buku Dipinjam</label>
                    <div class="col-sm-10">
                        <input name="book_id" id="book_id" class="form-control" value="{{ $transaction->buku->judul }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">Siswa Peminjam</label>
                    <div class="col-sm-10">
                        <input name="student_id" id="student_id" class="form-control" value="{{ $transaction->peminjam->nama }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">Tanggal Peminjaman</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" id="colFormLabel" name="tanggal_pinjam"
                            value="{{ $transaction->tanggal_pinjam }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">Durasi Peminjaman</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <input type="text" class="form-control" name="durasi" id="durasi"
                                aria-describedby="basic-addon3" value="{{ $transaction->durasi }}" disabled>
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon3">hari</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="colFormLabel" class="col-sm-2 col-form-label">Tanggal Pengembalian</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" id="colFormLabel" name="tanggal_kembali">
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Ubah</button>
            </form>
        </div>
    </div>


@endsection
