@extends('layout.master')

@section('judul')
    Transaksi Peminjaman
@endsection

@push('script')
    <script src="{{ asset('admin/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('admin/plugins/select2/css/select2.css') }}"></script>
    <script>
        $(function() {
            $("#select2").select2();
        });
    </script>
@endpush

@section('content')
    {{-- Button menambah peminjaman --}}
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalTambahPeminjaman">
        Tambah Peminjaman
    </button>

    {{-- Modal untuk menambah Penulis --}}
    <div class="modal fade" id="modalTambahPeminjaman" tabindex="-1" aria-labelledby="modalTambahPeminjamanLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalTambahPeminjamanLabel">Peminjaman Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/pinjam" method="POST">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label for="student_id">Nama Peminjam</label>
                            <select name="student_id" id="student_id" class="form-control">
                                <option value="">---Pilih Nama Peminjam---</option>
                                @foreach ($students as $student)
                                    <option value="{{ $student->id }}">{{ $student->nama }}, Kelas
                                        {{ $student->kelas }}</option>
                                @endforeach
                            </select>
                            @error('student_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="book_id">Nama Buku</label>
                            <select name="book_id" id="book_id" class="form-control">
                                <option value="">---Pilih Buku---</option>
                                @foreach ($books as $book)
                                    <option value="{{ $book->id }}">{{ $book->judul }}</option>
                                @endforeach
                            </select>
                            @error('book_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="user_name">Nama Petugas</label>
                            <input type="text" name="user_id" id="user_id" value="{{ $user->id }}" hidden>
                            <input type="text" name="user_name" id="user_name" value="{{ $user->name }}" disabled
                                class="form-control">
                            @error('user_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="tanggal_pinjam">Tanggal Peminjaman</label>
                            <input type="date" name="tanggal_pinjam" id="tanggal_pinjam" class="form-control">
                            @error('tanggal_pinjam')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="durasi">Durasi Pinjam</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" name="durasi" id="durasi"
                                    aria-describedby="basic-addon3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon3">hari</span>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- End modal untuk menambah Penulis --}}

    {{-- Tabel Transaksi --}}
    <div class="card my-3">
        <div class="card-body">

            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Buku</th>
                        <th scope="col">Peminjam</th>
                        <th scope="col">Kelas</th>
                        <th scope="col">Tanggal Pinjam</th>
                        <th scope="col">Durasi</th>
                        <th scope="col">Status</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($transactions as $item)
                        <tr
                            class="@if ($item->tanggal_kembali == null)
                            table-warning
                        @else
                        table-dark
                        @endif">
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $item->buku->judul }}</td>
                            <td>{{ $item->peminjam->nama }}</td>
                            <td>{{ $item->peminjam->kelas }}</td>
                            <td>{{ $item->tanggal_pinjam }}</td>
                            <td>{{ $item->durasi }}</td>
                            <td>{{ $item->status() }}</td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-info" href="/pinjam/{{ $item->id }}"><i
                                            class="far fa-eye"></i></a>
                                    <a class="btn btn-sm btn-secondary" href="/pinjam/{{ $item->id }}/edit"><i
                                            class="far fa-edit"></i></a>
                                    @if ($item->tanggal_kembali == null)
                                        <a class="btn btn-sm btn-success" href="/pinjam/{{ $item->id }}/return"><i
                                                class="fas fa-book"></i></a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>


        </div>
    </div>



@endsection
