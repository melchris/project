@extends('layout.master')

@section('judul')
    Ubah Detail Peminjaman
@endsection

@push('script')
    <script src="{{ asset('admin/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('admin/plugins/select2/css/select2.css') }}"></script>
    <script>
        $(function() {
            $("#select2").select2();
        });
    </script>
@endpush

@section('content')

    <div class="card my-3">
        <div class="card-body">
            <form action="/pinjam/{{ $transaction->id }}" method="POST">
                @csrf
                @method("PUT")
            <div class="form-group row">
                <label for="colFormLabel" class="col-sm-2 col-form-label">Buku Dipinjam</label>
                <div class="col-sm-10">
                    <select name="book_id" id="book_id" class="form-control">
                        <option value="">---Pilih Buku---</option>
                        @foreach ($books as $book)
                            <option value="{{ $book->id }}" @if ($book->id == $transaction->book_id)
                                selected
                            @endif>{{ $book->judul }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="colFormLabel" class="col-sm-2 col-form-label">Siswa Peminjam</label>
                <div class="col-sm-10">
                    <select name="student_id" id="student_id" class="form-control">
                        <option value="">---Pilih Nama Peminjam---</option>
                        @foreach ($students as $student)
                            <option value="{{ $student->id }}" @if ($student->id == $transaction->student_id)
                                selected
                            @endif>{{ $student->nama }}, Kelas
                                {{ $student->kelas }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="colFormLabel" class="col-sm-2 col-form-label">Tanggal Peminjaman</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" id="colFormLabel" name="tanggal_pinjam"
                        value="{{ $transaction->tanggal_pinjam }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="colFormLabel" class="col-sm-2 col-form-label">Durasi Peminjaman</label>
                <div class="col-sm-10">
                    <div class="input-group">
                        <input type="text" class="form-control" name="durasi" id="durasi" aria-describedby="basic-addon3" value="{{ $transaction->durasi }}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon3">hari</span>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-primary" type="submit">Ubah</button>
        </form>
        </div>
    </div>


@endsection
