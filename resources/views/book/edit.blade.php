@extends('layout.master')

@section('judul')
Halaman Tambah Buku {{$book->judul}}
@endsection

@section('content')
<form action="/book/{{ $book->id }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Judul Buku</label>
        <input type="text" name="judul" value={{$book->judul}} class="form-control">
    </div>
    @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Tahun Penerbit</label>
        <input type="number" name="tahun_terbit" value={{$book->tahun_terbit}} class="form-control">
    </div>
    @error('tahun_terbit')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Content</label>
        <textarea name="content" class="form-control" cols="30" rows="10">{{$book->content}}</textarea>
    </div>
    @error('content')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Penulis</label>
        <select name="penulis_id" value={{$book->penulis_id}} class="form-control">
            <option value="">---Pilih Kategori---</option>
            @foreach ($penulis as $item)
                @if ($item->id === $book->penulis_id)
                    <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                @else
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endif
            @endforeach
        </select>
    </div>
    @error('penulis_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Kategori</label>
        <select name="category_id" value={{$book->category_id}} class="form-control">
            <option value="">---Pilih Kategori---</option>
            @foreach ($category as $item)
                @if ($item->id === $book->category_id)
                    <option value="{{$item->id}}" selected>{{$item->name}}</option>
                @else
                    <option value="{{$item->id}}">{{$item->name}}</option>
                @endif
            @endforeach
        </select>
    </div>
    @error('category_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Penerbit</label>
        <select name="penerbit_id" value={{$book->penerbit_id}} class="form-control">
        <option value="">---Pilih Kategori---</option>
        @foreach ($penerbit as $item)
            @if ($item->id === $book->penerbit_id)
                <option value="{{$item->id}}" selected>{{$item->nama}}</option>
            @else
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endif
        @endforeach
        </select>
    </div>
    @error('penerbit_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Thumbnail</label>
        <input type="file" name="thumbnail" class="form-control">
    </div>
    @error('thumbnail')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection