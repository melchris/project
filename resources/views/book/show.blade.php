@extends('layout.master')

@section('judul')
Halaman Detail Buku {{$book->judul}}
@endsection

@push('script')
    <script src="{{ asset('admin/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('admin/plugins/select2/css/select2.css') }}"></script>
    <script>
        $(function() {
            $("#select2").select2();
        });
    </script>
@endpush

@section('content')
    @csrf
    @method('PUT')
    {{-- Button menghapus Buku --}}
    <div class="btn-group">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalHapuspenulis">
            Hapus
        </button>
        <a class="btn btn-secondary" href="/book/{{ $book->id }}/edit">Edit</a>
    </div>

    {{-- Modal untuk menghapus Buku --}}
    <div class="modal fade" id="modalHapuspenulis" tabindex="-1" aria-labelledby="modalHapuspenulisLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalHapuspenulisLabel">Hapus Buku</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/book/{{ $book->id }}" method="POST">
                    <div class="modal-body">
                        @csrf
                        @method('DELETE')
                        Yakin akan menghapus penulis?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Yakin Hapus</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- End modal untuk menghapus kategori --}}


    <div class="card my-3">
        <div class="card-body">

            <div class="form-group row">
                <label for="colFormLabel" class="col-sm-2 col-form-label">Nama Buku</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="colFormLabel" placeholder=" "
                        value="{{ $book->judul}}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="colFormLabel" class="col-sm-2 col-form-label">Tahun Terbit</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" id="colFormLabel" placeholder=" "
                        value="{{ $book->tahun_terbit }}" disabled>
                </div>
            </div>

            <div class="form-group row">
                <label for="colFormLabel" class="col-sm-2 col-form-label">Content</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="colFormLabel" placeholder=" "
                        value="{{ $book->content }}" disabled>
                </div>
            </div>

            <div class="form-group row">
                <label for="colFormLabel" class="col-sm-2 col-form-label">Penulis</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="colFormLabel" placeholder=" "
                        value="{{ $book->penulis->nama }}" disabled>
                </div>
            </div>

            <div class="form-group row">
                <label for="colFormLabel" class="col-sm-2 col-form-label">Kategori</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="colFormLabel" placeholder=" "
                        value="{{ $book->category->name }}" disabled>
                </div>
            </div>

            <div class="form-group row">
                <label for="colFormLabel" class="col-sm-2 col-form-label">Penerbit</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="colFormLabel" placeholder=" "
                        value="{{ $book->penerbit->nama }}" disabled>
                </div>
            </div>

            <div class="form-group row">
                <label for="colFormLabel" class="col-sm-2 col-form-label">Foto</label>
                <div class="col-sm-10">
                    {{-- <iframe width="240" height="160" src="{{ asset('gambar/' . $book->thumbnail) }}"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        class="border-0" allowfullscreen></iframe> --}}
                        <img src="{{ asset('gambar/' . $book->thumb()) }}" class="card-img-top" height="200px" width="250px" alt="">
                </div>
            </div>

        </div>
    </div>


@endsection
