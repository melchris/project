@extends('layout.master')

@section('judul')
    Halaman List Buku
@endsection

@push('script')
    <script src="{{ asset('admin/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('admin/plugins/select2/css/select2.css') }}"></script>
    <script>
        $(function() {
            $("#select2").select2();
        });
    </script>
@endpush

@section('content')

    @auth
        <a href="/book/create" class="btn btn-primary my-2">Tambah</a>
    @endauth
    {{-- <section class="content">
    <div class="container-fluid">
        <form action="">
            <ul class="list-unstyled">
                <li class="media">
                    <img src="..." class="mr-3" alt="...">
                    <div class="media-body">
                        <h5 class="mt-0 mb-1">List-based media object</h5>
                        <p>All my girls vintage Chanel baby. So you can have your cake. Tonight, tonight, tonight, I'm walking on air. Slowly swallowing down my fear, yeah yeah. Growing fast into a bolt of lightning. So hot and heavy, 'Til dawn. That fairy tale ending with a knight in shining armor. Heavy is the head that wears the crown.</p>
                    </div>
                </li>
        </form>
    </div>
</section> --}}
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">No</th>
                <th scope="col">Gambar</th>
                <th scope="col">Nama</th>
                <th scope="col">Tahun Terbit</th>
                <th scope="col">Penulis</th>
                <th scope="col">Penerbit</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($book as $key => $item)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    {{-- <td> </td> --}}
                    {{-- <td><iframe width="240" height="160" src="{{ asset('gambar/' . $item->thumbnail) }}"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            class="border-0" allowfullscreen></iframe></td> --}}
                    <td><img src="{{ asset('gambar/' . $item->thumb()) }}" class="card-img-top" height="200px" width="250px" alt=""></td>
                    <td>{{ $item->judul }}</td>
                    <td>{{ $item->tahun_terbit }}</td>
                    <td>{{ $item->penulis->nama }}</td>
                    <td>{{ $item->penerbit->nama }}</td>

                    <td>
                        <form action="/book/{{ $item->id }}" method="POST">
                            @method('delete')
                            @csrf
                            <div class="btn-group">
                                <a href="/book/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                                <a href="/book/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </div>
                            </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Data Masih Kosong</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
