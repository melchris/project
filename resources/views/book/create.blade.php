@extends('layout.master')

@section('judul')
Halaman Tambah Buku
@endsection

@section('content')
<form action="/book" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Judul Buku</label>
        <input type="text" name="judul" class="form-control">
    </div>
    @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Tahun Penerbit</label>
        <input type="number" name="tahun_terbit" class="form-control">
    </div>
    @error('tahun_terbit')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Content</label>
        <textarea name="content" class="form-control"  cols="30" rows="10"></textarea>
    </div>
    @error('content')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Penulis</label>
        <select name="penulis_id" class="form-control">
            <option value="">---Pilih Kategori---</option>
            @foreach ($penulis as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endforeach
        </select>
    </div>
    @error('penulis_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Kategori</label>
        <select name="category_id" class="form-control">
            <option value="">---Pilih Kategori---</option>
            @foreach ($category as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
            @endforeach
        </select>
    </div>
    @error('category_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Penerbit</label>
        <select name="penerbit_id" class="form-control">
        <option value="">---Pilih Kategori---</option>
        @foreach ($penerbit as $item)
            <option value="{{$item->id}}">{{$item->nama}}</option>
        @endforeach
        </select>
    </div>
    @error('penerbit_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Thumbnail</label>
        <input type="file" name="thumbnail" class="form-control">
    </div>
    @error('thumbnail')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
