@extends('layout.master')

@section('judul')
Selamat Datang
@endsection

@push('script')
<script src="{{asset('admin/plugins/select2/js/select2.js')}}"></script>
<script src="{{asset('admin/plugins/select2/css/select2.css')}}"></script>
<script>
<script>
    $(function () {
        $("#select2").select2();
    });
</script>
@endpush

@section('content')
    <p>Ini adalah layanan Perpustakan System. 
        Dimana aplikasi ini membantu petugas perpustakaan untuk mendata buku yang dimiliki, membuat list data siswa yang menjadi anggota perpus, mendata siswa yang meminjam dan mengembalikan buku. 
                Terima Kasih. </p>
@endsection