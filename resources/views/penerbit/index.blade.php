@extends('layout.master')

@section('judul')
    Penerbit Buku
@endsection

@push('script')
    <script src="{{ asset('admin/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('admin/plugins/select2/css/select2.css') }}"></script>
    <script>
        $(function() {
            $("#select2").select2();
        });
    </script>
@endpush

@section('content')
    {{-- Button menambah penerbit --}}
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalTambahPenerbit">
        Tambah Penerbit
    </button>

    {{-- Modal untuk menambah Penerbit --}}
    <div class="modal fade" id="modalTambahPenerbit" tabindex="-1" aria-labelledby="modalTambahPenerbitLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalTambahPenerbitLabel">Penerbit Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/penerbit" method="POST">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label for="nama">Nama Penerbit</label>
                            <input type="text" class="form-control" id="nama" name="nama" placeholder="Penerbit 1">
                            @error('nama')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <input type="text" class="form-control" id="alamat" name="alamat"
                                placeholder="Jalan 1 Nomor 2">
                            @error('alamat')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- End modal untuk menambah Penerbit --}}

    {{-- Tabel Modal --}}
    <div class="card my-3">
        <div class="card-body">

            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Penerbit</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($penerbit as $item)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $item->nama }}</td>
                            <td>{{ $item->alamat }}</td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-info" href="/penerbit/{{ $item->id }}"><i class="far fa-eye"></i></a>
                                    <a class="btn btn-sm btn-secondary" href="/penerbit/{{ $item->id }}/edit"><i class="far fa-edit"></i></a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>


        </div>
    </div>


@endsection
