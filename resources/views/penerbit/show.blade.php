@extends('layout.master')

@section('judul')
    Penerbit Buku
@endsection

@push('script')
    <script src="{{ asset('admin/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('admin/plugins/select2/css/select2.css') }}"></script>
    <script>
        $(function() {
            $("#select2").select2();
        });
    </script>
@endpush

@section('content')
    {{-- Button menghapus penerbit --}}
    <div class="btn-group">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalHapusPenerbit">
            Hapus
        </button>
        <a class="btn btn-secondary" href="/penerbit/{{ $penerbit->id }}/edit">Edit</a>
    </div>

    {{-- Modal untuk menghapus Penerbit --}}
    <div class="modal fade" id="modalHapusPenerbit" tabindex="-1" aria-labelledby="modalHapusPenerbitLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalHapusPenerbitLabel">Hapus Penerbit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/penerbit/{{ $penerbit->id }}" method="POST">
                    <div class="modal-body">
                        @csrf
                        @method('DELETE')
                        Yakin akan menghapus penerbit?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Yakin Hapus</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- End modal untuk menghapus Penerbit --}}


    <div class="card my-3">
        <div class="card-body">

            <div class="form-group row">
                <label for="colFormLabel" class="col-sm-2 col-form-label">Nama Penerbit</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="colFormLabel" placeholder=" "
                        value="{{ $penerbit->nama }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="colFormLabel" class="col-sm-2 col-form-label">Alamat Penerbit</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="colFormLabel" placeholder=" "
                        value="{{ $penerbit->alamat }}" disabled>
                </div>
            </div>


        </div>
    </div>


@endsection
