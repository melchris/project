@extends('layout.master')

@section('judul')
Halaman Tambah Kategori {{$category->name}}
@endsection


@section('content')
<form action="/category/{{$category->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama Kategori</label>
        <input type="text" name="name" value={{$category->name}} class="form-control">
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection