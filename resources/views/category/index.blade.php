@extends('layout.master')

@section('judul')
Halaman List Kategori
@endsection


@section('content')

<a href="/category/create" class="btn btn-success mb-3">Tambah Data</a>

<table class="table">
    <thead class="thead-dark">
        <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
       @forelse ($category as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->name}}</td>
                {{-- <td>
                    <ul>
                        @foreach ($item->book as $value)
                            <li>{{$value->name}}</li>
                        @endforeach
                    </ul>
                </td> --}}

                {{-- <td>
                    <form action="/category/{{$item->id}}" method="POST">
                        <a href="/category/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/category/{{$item->id}}/edit" class="btn btn-warning btn-sm ml-2">Edit</a>
                        @method('delete')
                        @csrf
                        <input type="submit" class="btn btn-danger btn-sm ml-2" value="Delete">
                    </form>
                </td> --}}
                <td>
                    <div class="btn-group">
                        <a class="btn btn-sm btn-info" href="/category/{{ $item->id }}"><i class="far fa-eye"></i></a>
                        <a class="btn btn-sm btn-secondary" href="/category/{{ $item->id }}/edit"><i class="far fa-edit"></i></a>
                    </div>
                </td>

            </tr>
       @empty
       <tr>
           <td>Data Masih Kosong</td>
       </tr>
       @endforelse
    </tbody>
</table>
@endsection
