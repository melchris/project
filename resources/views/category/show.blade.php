@extends('layout.master')

@section('judul')
Halaman Detail Kategori {{$category->name}}
@endsection

@push('script')
    <script src="{{ asset('admin/plugins/select2/js/select2.js') }}"></script>
    <script src="{{ asset('admin/plugins/select2/css/select2.css') }}"></script>
    <script>
        $(function() {
            $("#select2").select2();
        });
    </script>
@endpush

@section('content')
    {{-- Button menghapus kategori --}}
    <div class="btn-group">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalHapuspenulis">
            Hapus
        </button>
        <a class="btn btn-secondary" href="/category/{{ $category->id }}/edit">Edit</a>
    </div>

    {{-- Modal untuk menghapus kategori --}}
    <div class="modal fade" id="modalHapuspenulis" tabindex="-1" aria-labelledby="modalHapuspenulisLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalHapuspenulisLabel">Hapus Kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/category/{{ $category->id }}" method="POST">
                    <div class="modal-body">
                        @csrf
                        @method('DELETE')
                        Yakin akan menghapus penulis?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Yakin Hapus</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- End modal untuk menghapus kategori --}}


    <div class="card my-3">
        <div class="card-body">

            <div class="form-group row">
                <label for="colFormLabel" class="col-sm-2 col-form-label">Nama Kategori</label>
                <div class="col-sm-10"> 
                    <input type="text" class="form-control" id="colFormLabel" placeholder=" "
                        value="{{ $category->name }}" disabled>
                </div>
            </div>
        </div>
    </div>


@endsection